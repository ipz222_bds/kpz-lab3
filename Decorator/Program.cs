﻿class Program
{
    static void Main(string[] args)
    {
        IInventory warrior = new Warrior();
        IInventory warriorWithSword = new Sword(warrior);
        IInventory warriorWithSwordAndArmor = new Armor(warriorWithSword);

        warriorWithSwordAndArmor.Equip();
    }
}
