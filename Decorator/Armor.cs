using System;

public class Armor : InventoryDecorator
{
    public Armor(IInventory inventory) : base(inventory)
    {
    }

    public override void Equip()
    {
        base.Equip();
        Console.WriteLine("Armor equipped");
    }
}
