public abstract class InventoryDecorator : IInventory
{
    protected IInventory _inventory;

    public InventoryDecorator(IInventory inventory)
    {
        _inventory = inventory;
    }

    public virtual void Equip()
    {
        _inventory?.Equip();
    }
}
