using System;

public class Sword : InventoryDecorator
{
    public Sword(IInventory inventory) : base(inventory)
    {
    }

    public override void Equip()
    {
        base.Equip();
        Console.WriteLine("Sword equipped");
    }
}
