using System;

public class Artifact : InventoryDecorator
{
    public Artifact(IInventory inventory) : base(inventory)
    {
    }

    public override void Equip()
    {
        base.Equip();
        Console.WriteLine("Artifact equipped");
    }
}
