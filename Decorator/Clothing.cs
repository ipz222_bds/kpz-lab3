using System;

public class Clothing : InventoryDecorator
{
    public Clothing(IInventory inventory) : base(inventory)
    {
    }

    public override void Equip()
    {
        base.Equip();
        Console.WriteLine("Clothing equipped");
    }
}
