using System;

public class Weapon : InventoryDecorator
{
    public Weapon(IInventory inventory) : base(inventory)
    {
    }

    public override void Equip()
    {
        base.Equip();
        Console.WriteLine("Weapon equipped");
    }
}
