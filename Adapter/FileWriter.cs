using System.IO;

public class FileWriter
{
    private readonly string _filename;

    public FileWriter(string filename)
    {
        _filename = filename;
    }

    public void Write(string message)
    {
        File.AppendAllText(_filename, message);
    }

    public void WriteLine(string message)
    {
        File.AppendAllText(_filename, message + Environment.NewLine);
    }
}
