public class FileLogger
{
    private readonly Logger _logger;
    private readonly FileWriter _fileWriter;

    public FileLogger(string filename)
    {
        _logger = new Logger();
        _fileWriter = new FileWriter(filename);
    }

    public void Log(string message)
    {
        _logger.Log(message);
        _fileWriter.WriteLine($"LOG: {message}");
    }

    public void Error(string message)
    {
        _logger.Error(message);
        _fileWriter.WriteLine($"ERROR: {message}");
    }

    public void Warn(string message)
    {
        _logger.Warn(message);
        _fileWriter.WriteLine($"WARN: {message}");
    }
}
