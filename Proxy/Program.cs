﻿using System;

class Program
{
    static void Main(string[] args)
    {
        string filePath = "/Users/dimabalenko/Documents/другий курс/КПЗ/Proxy/test.txt";

        Console.WriteLine("Testing SmartTextReader:");
        SmartTextReader reader = new SmartTextReader(filePath);
        char[][] textArray = reader.ReadTextFile();
        if (textArray != null)
        {
            foreach (var line in textArray)
            {
                Console.WriteLine(new string(line));
            }
        }

        Console.WriteLine("\nTesting SmartTextChecker:");
        SmartTextChecker checker = new SmartTextChecker(filePath);
        textArray = checker.ReadTextFile();

        Console.WriteLine("\nTesting SmartTextReaderLocker:");
        SmartTextReaderLocker locker = new SmartTextReaderLocker(filePath, @"test\.txt");
        textArray = locker.ReadTextFile();

        Console.ReadKey();
    }
}
