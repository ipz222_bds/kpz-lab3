using System;
using System.IO;

public class SmartTextReader
{
    protected string filePath;

    public SmartTextReader(string filePath)
    {
        this.filePath = filePath;
    }

    public char[][] ReadTextFile()
    {
        try
        {
            string[] lines = File.ReadAllLines(filePath);
            char[][] charArray = new char[lines.Length][];

            for (int i = 0; i < lines.Length; i++)
            {
                charArray[i] = lines[i].ToCharArray();
            }

            return charArray;
        }
        catch (Exception ex)
        {
            Console.WriteLine("An error occurred while reading the file: " + ex.Message);
            return null;
        }
    }
}
