using System;
using System.Text.RegularExpressions;

public class SmartTextReaderLocker : SmartTextReader
{
    private Regex restrictedFilesRegex;

    public SmartTextReaderLocker(string filePath, string restrictedPattern) : base(filePath)
    {
        restrictedFilesRegex = new Regex(restrictedPattern);
    }

    public new char[][] ReadTextFile()
    {
        if (restrictedFilesRegex.IsMatch(filePath))
        {
            Console.WriteLine("Access denied!");
            return null;
        }
        else
        {
            return base.ReadTextFile();
        }
    }
}
