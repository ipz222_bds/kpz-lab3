using System;

public class SmartTextChecker : SmartTextReader
{
    public SmartTextChecker(string filePath) : base(filePath)
    {
    }

    public new char[][] ReadTextFile()
    {
        Console.WriteLine("Opening file: " + filePath);
        char[][] result = base.ReadTextFile();
        if (result != null)
        {
            Console.WriteLine("File read successfully.");
            Console.WriteLine("Number of lines: " + result.Length);
            int totalChars = 0;
            foreach (var line in result)
            {
                totalChars += line.Length;
            }
            Console.WriteLine("Total number of characters: " + totalChars);
        }
        Console.WriteLine("Closing file: " + filePath);
        return result;
    }
}
