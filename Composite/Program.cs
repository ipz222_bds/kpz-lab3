﻿using System;

class Program
{
    static void Main(string[] args)
    {
        LightElementNode div = new LightElementNode("div", true, false);
        div.CssClasses.Add("container");

        LightElementNode p = new LightElementNode("p", true, false);
        p.Children.Add(new LightTextNode("This is a paragraph."));
        
        LightElementNode img = new LightElementNode("img", false, true);
        img.CssClasses.Add("responsive");

        div.Children.Add(p);
        div.Children.Add(img);

        Console.WriteLine(div.OuterHTML);
    }
}
