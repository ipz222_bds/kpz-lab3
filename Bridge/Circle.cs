// Клас для відрисування круга
public class Circle : Shape
{
    public Circle(IRenderer renderer) : base(renderer)
    {
    }

    public override void Draw()
    {
        renderer.RenderCircle();
    }
}
