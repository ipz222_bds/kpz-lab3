// Абстрактний клас для роботи з фігурами
public abstract class Shape
{
    protected IRenderer renderer;

    // Конструктор, який приймає об'єкт рендерера
    protected Shape(IRenderer renderer)
    {
        this.renderer = renderer;
    }

    // Абстрактний метод для відображення фігури
    public abstract void Draw();
}
