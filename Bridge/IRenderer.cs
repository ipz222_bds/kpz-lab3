using System;

// Інтерфейс рендерера
public interface IRenderer
{
    void RenderCircle();
    void RenderSquare();
    void RenderTriangle();
}
