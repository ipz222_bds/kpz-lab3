﻿using System;

class Program
{
    static void Main(string[] args)
    {
        // Створюємо растровий рендерер
        var rasterRenderer = new RasterRenderer();
        // Створюємо квадрат і рендеримо його растрово
        var square = new Square(rasterRenderer);
        square.Draw();

        // Створюємо векторний рендерер
        var vectorRenderer = new VectorRenderer();
        // Створюємо трикутник і рендеримо його векторно
        var triangle = new Triangle(vectorRenderer);
        triangle.Draw();
    }
}
