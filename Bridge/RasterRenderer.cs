using System;

// Клас для растрового рендерера
public class RasterRenderer : IRenderer
{
    public void RenderCircle()
    {
        Console.WriteLine("Drawing Circle as pixels");
    }

    public void RenderSquare()
    {
        Console.WriteLine("Drawing Square as pixels");
    }

    public void RenderTriangle()
    {
        Console.WriteLine("Drawing Triangle as pixels");
    }
}
