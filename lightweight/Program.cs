﻿using System;
using System.Collections.Generic;

namespace Task6
{
    class Program
    {
        static void Main(string[] args)
        {
            LightElementNode div = new LightElementNode("div", true, false);
            div.CssClasses.Add("container");

            LightElementNode p = new LightElementNode("p", true, false);
            p.Children.Add(new LightTextNode("This is a paragraph."));
            
            LightElementNode img = new LightElementNode("img", false, true);
            img.CssClasses.Add("responsive");

            div.Children.Add(p);
            div.Children.Add(img);

            Console.WriteLine(div.OuterHTML);

            // Приклад тексту книги
            string bookText = @"
Заголовок книги
Передмова
Текст абзацу з вмістом книги, який досить довгий, щоб бути абзацом.
Короткий текст
 Продовження книги.
";

            // Виклик методу для перетворення тексту книги
            var htmlTree = ConvertBookTextToHtml(bookText);
            Console.WriteLine(htmlTree.OuterHTML);

            // Вимірювання споживання пам'яті
            long memoryBefore = GC.GetTotalMemory(true);
            var htmlTreeForMemory = ConvertBookTextToHtml(bookText);
            long memoryAfter = GC.GetTotalMemory(true);

            Console.WriteLine($"Memory used: {memoryAfter - memoryBefore} bytes");
        }

       static LightElementNode ConvertBookTextToHtml(string bookText)
{
    var factory = new LightElementNodeFactory();
    var lines = bookText.Split(new[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);
    LightElementNode root = factory.GetElement("div", true, false);

    bool isFirstLine = true;

    foreach (var line in lines)
    {
        string trimmedLine = line.Trim();
        if (string.IsNullOrEmpty(trimmedLine)) continue;

        LightElementNode element;

        if (isFirstLine)
        {
            element = factory.GetElement("h1", true, false);
            isFirstLine = false;
        }
        else if (trimmedLine.StartsWith(" "))
        {
            element = factory.GetElement("blockquote", true, false);
        }
        else if (trimmedLine.Length < 20)
        {
            element = factory.GetElement("h2", true, false);
        }
        else
        {
            element = factory.GetElement("p", true, false);
        }

        element.Children.Add(new LightTextNode(trimmedLine)); // Додаємо кожен рядок як дочірній елемент до відповідного тегу
        root.Children.Add(element);
    }

    return root;
}




    }
}
