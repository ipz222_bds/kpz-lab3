using System.Collections.Generic;

namespace Task6
{
    public class LightElementNodeFactory
    {
        private Dictionary<string, LightElementNode> _elementPool = new Dictionary<string, LightElementNode>();

        public LightElementNode GetElement(string tagName, bool isBlock, bool isSelfClosing)
        {
            string key = $"{tagName}-{isBlock}-{isSelfClosing}";
            if (!_elementPool.ContainsKey(key))
            {
                _elementPool[key] = new LightElementNode(tagName, isBlock, isSelfClosing);
            }
            return _elementPool[key];
        }
    }
}
